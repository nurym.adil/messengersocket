﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Domain;
namespace MessengerClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        public ObservableCollection<Message> Messages { get; set; }
        public string Name { get; set; }
        public Client Client { get; set; }


        public MainWindow()
        {
            InitializeComponent();

            Messages = new ObservableCollection<Message>();
            chatListBox.ItemsSource = Messages;

        }

        private async void UpdateMessage()
        {
            try
            {

                var messages = await Task.Run(() =>
                  {
                      ObservableCollection<Message> collection = new ObservableCollection<Message>();
                      collection.Add(Client.ReceiveMessageAsync().Result);
                      return collection;
                  });
                foreach (var message in messages)
                {
                    Messages.Add(message);
                }
                AutoScroll();
                UpdateMessage();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
            }

        }

        private void AutoScroll()
        {
            chatListBox.SelectedIndex = chatListBox.Items.Count - 1;
            chatListBox.ScrollIntoView(chatListBox.SelectedItem);
        }

        private async void SendButton(object sender, RoutedEventArgs e)
        {
            if (messageTextBox.Text != string.Empty)
            {
                await Client.SendMessageAsync(Name, messageTextBox.Text);
            }
            else
            {
                MessageBox.Show("Введите сообщение!");
            }
        }


        private async void Connect(object sender, RoutedEventArgs e)
        {
            if (userNameTextBox.Text != string.Empty)
            {
                try
                {
                    Client = new Client();
                    await Client.ConnectAsync();
                    UpdateMessage();
                    Name = userNameTextBox.Text;
                    sendButton.IsEnabled = true;
                    connect.Visibility = Visibility.Hidden;
                    connect.IsEnabled = false;
                }
                catch (SocketException)
                {
                    MessageBox.Show("Подключение не удалось");
                }
            }
            else
            {
                MessageBox.Show("Введите свое имя:");
            }
        }

    }
}
