﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MessengerClientConsole
{
    class Program
    {
        public static IPEndPoint IPEndPoint { get; set; } = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 3031);
        private static Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        static async Task Main(string[] args)
        {
            await ConnectAsync();
           await RequestAsync();
            Console.ReadLine();

        }

        private static async Task ConnectAsync()
        {
            while (!socket.Connected)
            {
                try
                {
                    await socket.ConnectAsync(IPEndPoint);
                }
                catch (SocketException)
                {
                    Console.WriteLine("Неудачная попытка");
                    Console.WriteLine("Повторяем попытку");
                    Console.Clear();
                }
            }

            Console.WriteLine("Успешно подключено");
        }

        private static async Task RequestAsync()
        {
            while (true)
            {
                await SendMessageAsync();
                Console.ReadLine();
                await ReceiveMessageAsync();
            }
        }

        private static async Task SendMessageAsync()
        {
            Console.WriteLine("Введите сообщение:");
            byte[] buffer = Encoding.UTF8.GetBytes(Console.ReadLine());
            await socket.SendAsync(buffer,SocketFlags.None);
        }

        private static async Task ReceiveMessageAsync()
        {
            byte[] buffer = new byte[1024];
            int received = await socket.ReceiveAsync(buffer, SocketFlags.None);
            if (received == 0)
            {
                return;
            }
            byte[] data = new byte[received];
            Array.Copy(buffer, data, received);
            Console.WriteLine(Encoding.UTF8.GetString(data));
        }
        //private static void Start()
        //{
        //    socket.BeginConnect(IPEndPoint, Connect, null);
        //}

        //private static void Connect(IAsyncResult ar)
        //{
        //    Socket client = ar.AsyncState as Socket;
        //    client.EndConnect(ar);

        //    client.BeginSend()
        //}
    }
}
