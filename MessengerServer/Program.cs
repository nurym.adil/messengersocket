﻿using Domain;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MessengerServer
{
    class Program
    {
        static void Main(string[] args)
        {
            using (Host host = new Host())
            {
                host.Start();
                Console.ReadLine();
            }
        }


        //private static void Send(IAsyncResult ar)
        //{
        //    Socket sendSocket = ar.AsyncState as Socket;

        //    sendSocket.EndSend(ar);
        //}
    }
}
