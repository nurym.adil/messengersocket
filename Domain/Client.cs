﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Client
    {
        public ICollection<string> Messages = new List<string>();

        public static IPEndPoint IPEndPoint { get; set; } = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 3031);
        private static Socket Socket { get; set; } = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        public ByteArrayConvertor ByteArrayConvertor { get; set; } = new ByteArrayConvertor();
        public async Task SendMessageAsync(string name, string text)
        {

            Message message = new Message
            {
                User = name,
                Text = text
            };
            byte[] buffer = ByteArrayConvertor.ObjectToByteArray(message); ;
            await Socket.SendAsync(buffer, SocketFlags.None);
        }
        public async Task ConnectAsync()
        {
            await Socket.ConnectAsync(IPEndPoint);
            //  await Socket.SendAsync(Encoding.UTF8.GetBytes(name), SocketFlags.None);
        }
        public async Task<Message> ReceiveMessageAsync()
        {
            int bufferSize = 2048;
            byte[] buffer = new byte[bufferSize];
            int received = await Socket.ReceiveAsync(buffer, SocketFlags.None);
            if (received == 0)
            {
                return null;
            }
            byte[] data = new byte[received];
            Array.Copy(buffer, data, received);

            var temp = ByteArrayConvertor.ByteArrayToObject(data);
            Message message = temp as Message;
            return message as Message;
        }
        public void Close()
        {
            if (Socket != null)
            {
                Socket.Close();
            }
        }
    }
}
