﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Host : IDisposable
    {

        public static IPEndPoint IPEndPoint { get; set; } = new IPEndPoint(IPAddress.Any, 3031);
        private static Socket Socket { get; set; } = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private static List<Socket> Clients { get; set; } = new List<Socket>();
        private const int BUFFER_LENGTH = 1024;
        private static readonly byte[] buffer = new byte[BUFFER_LENGTH];
        public ByteArrayConvertor ByteArrayConvertor { get; set; } = new ByteArrayConvertor();

        public void Start()
        {
            Console.WriteLine("Запуск");

            Socket.Bind(IPEndPoint);
            Socket.Listen(100);
            Socket.BeginAccept(AcceptClients, null);
        }

        private void AcceptClients(IAsyncResult ar)
        {
            Socket client;
            try
            {

                client = Socket.EndAccept(ar);
            }
            catch (ObjectDisposedException)
            {
                return;
            }

            Clients.Add(client);
            client.BeginReceive(buffer, 0, BUFFER_LENGTH, SocketFlags.None, Receive, client);
            Console.WriteLine($"Соединение установлено c клиентом :{client.AddressFamily}");
            Socket.BeginAccept(AcceptClients, null);
        }

        private async void Receive(IAsyncResult ar)
        {
            Socket client = ar.AsyncState as Socket;
            int received;

            try
            {
                received = client.EndReceive(ar);
            }
            catch (SocketException)
            {
                Console.WriteLine("Клиент вышел из сети");
                client.Close();
                Clients.Remove(client);
                return;
            }
            byte[] data = new byte[received];
            Array.Copy(buffer, data, received);

            var temp = ByteArrayConvertor.ByteArrayToObject(data);
            Message message = temp as Message;
        await    AddDatabaseAsync(message);
            Console.WriteLine($"{message.User}:{message.Text}");
            await BroadcastAsync(data);
            client.BeginReceive(buffer, 0, BUFFER_LENGTH, SocketFlags.None, Receive, client);
        }

        private async Task AddDatabaseAsync(Message message)
        {
            using (var context = new Context())
            {
                await context.AddAsync(message);
                await context.SaveChangesAsync();
            }
        }

        private async Task BroadcastAsync(byte[] data)
        {
            foreach (var client in Clients)
            {
                await client.SendAsync(data, SocketFlags.None);
            }
        }

        public void Close()
        {
            foreach (var client in Clients)
            {
                client.Close();
            }
            Socket.Close();
        }

        public void Dispose()
        {
            Close();
        }
    }
}
