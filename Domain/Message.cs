﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
        [Serializable]
    public class Message
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string User { get; set; }
        public string Text { get; set; }
        public DateTime CreationDate { get; set; } = DateTime.Now;
        public override string ToString()
        {
            return $"[{CreationDate.Hour}:{CreationDate.Minute}:{CreationDate.Second}] {User}:{Text}";
        }

    }
}
